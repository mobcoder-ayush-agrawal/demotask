import { BrowserRouter as Router , Route,Routes, } from "react-router-dom";
import Login from "./auth/Login";
import Signup from "./auth/Signup";
import BoardDetails from "./Pages/BoardDetails";
import Myboard from "./Pages/Myboard";
function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path='/' element={<Signup/>} />
          <Route path='/login' element={<Login/>} />
          <Route path='/myboard' element={<Myboard/>} />
          <Route path='/myboard/boarddetails' element={<BoardDetails/>} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;

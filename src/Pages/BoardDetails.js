import React, { useEffect, useState } from "react";

export default function BoardDetails() {
  const [boarddetails, setboarddetails] = useState([
    {
      id: 0,
      TaskName: "Task0",
      Status: "Todo",
    },
    {
      id: 1,
      TaskName: "Task1",
      Status: "Inprogress",
    },
    {
      id: 2,
      TaskName: "Task2",
      Status: "Onhold",
    },
    {
      id: 3,
      TaskName: "Task3",
      Status: "Completed",
    },
    {
      id: 4,
      TaskName: "Task4",
      Status: "Released",
    },
    {
      id: 5,
      TaskName: "Task5",
      Status: "Inprogress",
    },
    {
      id: 6,
      TaskName: "Task6",
      Status: "Onhold",
    },
    {
      id: 7,
      TaskName: "Task7",
      Status: "Todo",
    },
  ]);
  const [draggeditem, setdraggeditem] = useState([]);
  const [todo, settodo] = useState([]);
  const [inprogress, setinprogress] = useState([]);
  const [onhold, setonhold] = useState([]);
  const [released, setreleased] = useState([]);
  const [completed, setcompleted] = useState([]);
  useEffect(() => {
    const Todo = boarddetails.filter((word) => {
      if (word.Status == "Todo") {
        return <div>{word.TaskName}</div>;
      }
    });
    const Inprogress = boarddetails.filter((word) => {
      if (word.Status == "Inprogress") {
        return <div>{word.TaskName}</div>;
      }
    });
    const Onhold = boarddetails.filter((word) => {
      if (word.Status == "Onhold") {
        return <div>{word.TaskName}</div>;
      }
    });
    const Released = boarddetails.filter((word) => {
      if (word.Status == "Released") {
        return <div>{word.TaskName}</div>;
      }
    });
    const Completed = boarddetails.filter((word) => {
      if (word.Status == "Completed") {
        return <div>{word.TaskName}</div>;
      }
    });

    settodo(Todo);
    setinprogress(Inprogress);
    setonhold(Onhold);
    setreleased(Released);
    setcompleted(Completed);
  }, [boarddetails]);

  const dragfile = (e) => {
    e.preventDefault();
  };
  const dragstart = (e, item) => {
    setdraggeditem(item);
  };
  const ondrop = (e, value) => {
    let removeditem = boarddetails.filter((item) => item.id !== draggeditem.id);
    let newitem = { ...draggeditem, Status: value };
    removeditem.push(newitem);
    setboarddetails(removeditem);
  };
  console.log(draggeditem)
  return (
    <div style={{ display: "flex", flexDirection: "row", gap: "2%" }}>
      <div>
        <h1>Todo</h1>
        <div
          onDragOver={(e) => dragfile(e)}
          onDrop={(e) => {
            ondrop(e, "Todo");
          }}
        >
          {todo.map((item) => (
            <p
              style={{
                backgroundColor: "#c7c4c4",
                textAlign: "center",
                borderRadius: "2px",
                cursor: "pointer",
              }}
              draggable
              onDragStart={(e) => dragstart(e, item)}
            >
              {item.TaskName}
            </p>
          ))}
        </div>
      </div>
      <div
        onDragOver={(e) => dragfile(e)}
        onDrop={(e) => {
          ondrop(e, "Inprogress");
        }}
      >
        <h1>Inprogress</h1>
        {inprogress.map((item) => (
          <p
            style={{
              backgroundColor: "#c7c4c4",
              textAlign: "center",
              borderRadius: "2px",
              cursor: "pointer",
            }}
            draggable
            onDragStart={(e) => dragstart(e, item)}
          >
            {item.TaskName}
          </p>
        ))}
      </div>
      <div
        onDragOver={(e) => dragfile(e)}
        onDrop={(e) => {
          ondrop(e, "Onhold");
        }}
      >
        <h1>Onhold</h1>
        {onhold.map((item) => (
          <p
            style={{
              backgroundColor: "#c7c4c4",
              textAlign: "center",
              borderRadius: "2px",
              cursor: "pointer",
            }}
            draggable
            onDragStart={(e) => dragstart(e, item)}
          >
            {item.TaskName}
          </p>
        ))}
      </div>
      <div
        onDragOver={(e) => dragfile(e)}
        onDrop={(e) => {
          ondrop(e, "Released");
        }}
      >
        <h1>Released</h1>
        {released.map((item) => (
          <p
            style={{
              backgroundColor: "#c7c4c4",
              textAlign: "center",
              borderRadius: "2px",
              cursor: "pointer",
            }}
            draggable
            onDragStart={(e) => dragstart(e, item)}
          >
            {item.TaskName}
          </p>
        ))}
      </div>
      <div
        onDragOver={(e) => dragfile(e)}
        onDrop={(e) => {
          ondrop(e, "Complete");
        }}
      >
        <h1>Complete</h1>
        {completed.map((item) => (
          <p
            style={{
              backgroundColor: "#c7c4c4",
              textAlign: "center",
              borderRadius: "2px",
              cursor: "pointer",
            }}
            draggable
            onDragStart={(e) => dragstart(e, item)}
          >
            {item.TaskName}
          </p>
        ))}
      </div>
    </div>
  );
}

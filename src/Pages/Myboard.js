import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./Myboard.css";
export default function Myboard() {
  const [boardname, setboardname] = useState()
  const [boardcolor, setboardcolor] = useState()
  const [displaytiles, setdisplaytiles] = useState([])
  const handleboard = () => {
      setdisplaytiles([...displaytiles,{BoardName:boardname,BoardColor:boardcolor}])
  };
  console.log(displaytiles)
  const DiplayTile=()=>{
      return( displaytiles.map(item=>{return(<Link to='/myboard/boarddetails'><div class="card" style={{width: '18rem',backgroundColor:`${item.BoardColor}`}}>
      <div class="card-body">
        <h5 class="card-title" >Board title</h5>
        {item.BoardName}
        
      </div></div> </Link>
    )})
) }
  return (
    <div className="board-content">
      <h3>Create a New Board</h3>
      <hr></hr>
      <div className="Distributedcard">
      {DiplayTile()}
      <i
        class="fas fa-plus"
        style={{ fontSize: "32px",paddingTop:'2%' }}
        type="button"
        data-bs-toggle="modal"
        value='defaultvalue'
        data-bs-target="#exampleModal"
      ></i>
      </div>
      <div
        class="modal fade"
        id="exampleModal"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <div>
                <h5 class="modal-title" id="exampleModalLabel">
                  Board Title
                </h5>
              </div>
              <br></br>
              <input
                type="text"
                class="form-control"
                id='defaultinput'
                placeholder="Board Name"
                aria-label="Username"
                aria-describedby="basic-addon1"
                onChange={(e)=>{
                    setboardname(e.target.value)
                }}
              />
            </div>
            <div class="modal-body">
              <h5>Choose Board Color</h5>
            </div>
            <input type="color" id="favcolor" name="favcolor" value="#ff0000"onChange={e=>{setboardcolor(e.target.value)}}/>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Close
              </button>
              <button type="button" class="btn btn-primary" onClick={handleboard} data-bs-dismiss="modal">
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

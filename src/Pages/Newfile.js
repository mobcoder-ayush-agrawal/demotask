import { useEffect, useState } from 'react';

const _taskList = [
  {id: 1, taskName: 'task 1', status: 1},
  {id: 2, taskName: 'task 2', status: 1},
  {id: 3, taskName: 'task 3', status: 2},
  {id: 4, taskName: 'task 4', status: 2},
  {id: 5, taskName: 'task 5', status: 4},
  {id: 6, taskName: 'task 6', status: 1},
  {id: 7, taskName: 'task 7', status: 2},
  {id: 8, taskName: 'task 8', status: 1},
  {id: 9, taskName: 'task 9', status: 4},
  {id: 10, taskName: 'task 10', status: 1},
  {id: 11, taskName: 'task 11', status: 2},
  {id: 12, taskName: 'task 12', status: 5},
  {id: 13, taskName: 'task 13', status: 5},
  {id: 14, taskName: 'task 14', status: 5},
  {id: 15, taskName: 'task 15', status: 2},
  {id: 16, taskName: 'task 16', status: 3},
  {id: 17, taskName: 'task 17', status: 2},
  {id: 18, taskName: 'task 18', status: 2},
  {id: 19, taskName: 'task 19', status: 1},
  {id: 20, taskName: 'task 20', status: 1}
]

// status
// To do  = 1
// In Progress = 2
// On hold = 3
// Completed = 4
// Released = 5

const App = () => {
  const [taskList, setTaskList] = useState(_taskList);
  const [todoList, setTodoList] = useState([]);
  const [inProgressList, setInProgressList] = useState([]);
  const [onHoldList, setOnHoldList] = useState([]);
  const [completedList, setCompletedList] = useState([]);
  const [releasedList, setReleasedList] = useState([]);
  const [dragTask, setDragTask] = useState();


  useEffect(() => {
    let _todoList = taskList.filter(i => i.status == 1);
    let _inProgressList = taskList.filter(i => i.status == 2);
    let _onHoldList = taskList.filter(i => i.status == 3);
    let _completedList = taskList.filter(i => i.status == 4);
    let _releasedList = taskList.filter(i => i.status == 5);

    setTodoList(_todoList);
    setInProgressList(_inProgressList);
    setOnHoldList(_onHoldList);
    setCompletedList(_completedList);
    setReleasedList(_releasedList);
    setDragTask();
  }, [taskList])

  const onDragStart = (e, item) => {
    setDragTask(item);
  }

  const onDragOver = e => {
    e.preventDefault();
  }

  const onDrop = (e, cat) => {
    let tempTasks = taskList.filter(i => i.id != dragTask.id);
    let _dragTask = {...dragTask, status: cat};
    tempTasks.push(_dragTask);
    setTaskList(tempTasks);
  }

  return (
    <div className="App">
      <div className="borad_wrapper">

          <div className="borad">
            <h3>To do</h3>
            <div
              onDragOver={(event)=>onDragOver(event)}
              onDrop={event => onDrop(event, 1)}>
                {todoList.map(item => <div id={item.id} onDragStart={(event) => onDragStart(event, item)} draggable className="draggable" key={item.id}>{item.taskName}</div>)}
            </div>
          </div>


          <div className="borad">
            <h3>In Progress</h3>
            <div
              onDragOver={(event)=>onDragOver(event)}
              onDrop={event=> onDrop(event, 2)}>
                {inProgressList.map(item => <div onDragStart={(event) => onDragStart(event, item)} draggable className="draggable" key={item.id}>{item.taskName}</div>)}
            </div>
          </div>


          <div className="borad">
          <h3>On hold</h3>
            <div
              onDragOver={(event)=>onDragOver(event)}
              onDrop={event=> onDrop(event, 3)}>
                {onHoldList.map(item => <div onDragStart={(event) => onDragStart(event, item)} draggable className="draggable"key={item.id}>{item.taskName}</div>)}
            </div>
          </div>

          <div className="borad">
          <h3>Completed</h3>
            <div
              onDragOver={(event)=>onDragOver(event)}
              onDrop={event=> onDrop(event, 4)}>
                {completedList.map(item => <div onDragStart={(event) => onDragStart(event, item)} draggable className="draggable" key={item.id}>{item.taskName}</div>)}
            </div>
          </div>

          <div className="borad">
          <h3>Completed</h3>
            <div
              onDragOver={(event)=>onDragOver(event)}
              onDrop={event=> onDrop(event, 5)}>
                {releasedList.map(item => <div onDragStart={(event) => onDragStart(event, item)} draggable className="draggable" key={item.id}>{item.taskName}</div>)}
            </div>
          </div>

        </div>
    </div>
  );
}

export default App;
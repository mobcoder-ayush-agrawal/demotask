import React from "react";
import { Link, useNavigate } from "react-router-dom";

export default function Login() {
const Navi = useNavigate()


const handlelogin=()=>{
Navi('/myboard')
}




  return (
    <div className="main-content">
      <div class="card" style={{ width: "18rem" }}>
        <div class="card-body">
          <h4>Login</h4>
          <div class="input-group mb-3">
            <div className="email-field">
              <input
                type="text"
                class="form-control"
                placeholder="Username"
                aria-label="Username"
                aria-describedby="basic-addon1"
              />
            </div>
            <div>
              <input
                type="password"
                class="form-control"
                placeholder="Password"
                aria-label="Username"
                aria-describedby="basic-addon1"
              />
            </div>
            <div style={{ paddingTop: "2%", paddingBottom: "2%" }} >
              <button className="btn btn-large btn-block btn-primary" onClick={handlelogin}>Login</button>
            </div>
            <div>
                <Link to='/'> Create a New Account</Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

import React from "react";
import { Link } from "react-router-dom";
import "./Signup.css";
export default function Signup() {
  return (
    <div className="main-content">
      <div class="card" style={{ width: "18rem" }}>
        <div class="card-body">
          <h4>Signup for Free</h4>
          <div class="input-group mb-3">
            <div className="email-field">
              <input
                type="text"
                class="form-control"
                placeholder="Username"
                aria-label="Username"
                aria-describedby="basic-addon1"
              />
            </div>
            <div>
              <input
                type="password"
                class="form-control"
                placeholder="Password"
                aria-label="Username"
                aria-describedby="basic-addon1"
              />
            </div>
            <div style={{paddingTop:'2%',paddingBottom:'2%'}}>
                <button className="btn btn-primary">SignUp</button>
            </div >
            <div>
                <Link to='/login'> Already Have an Account</Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
